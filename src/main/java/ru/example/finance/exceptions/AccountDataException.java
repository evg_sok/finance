package ru.example.finance.exceptions;

public class AccountDataException extends Exception {
    public AccountDataException(String message) {
        super(message);
    }
}
