package ru.example.finance.util;

import java.util.Random;

public class Helper {
    public static String generateAccount() {
        Random rand = new Random();

        StringBuilder accountNum = new StringBuilder();
        for (int i = 0; i < 16; i++) accountNum.append(rand.nextInt(10));
        return accountNum.toString();
    }
}
