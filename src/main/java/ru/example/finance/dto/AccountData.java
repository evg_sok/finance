package ru.example.finance.dto;

import lombok.Data;
import ru.example.finance.entities.Account;

import java.time.LocalDateTime;

@Data
public class AccountData {
    private String num;
    private Float cash;
    private LocalDateTime updateAt;

    public AccountData() {
    }
    public AccountData(Account account) {
        this.num = account.getNum();
        this.cash = account.getCash();
        this.updateAt = account.getUpdatedAt();
    }
}
