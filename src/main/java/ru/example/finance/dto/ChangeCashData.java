package ru.example.finance.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class ChangeCashData {
    @NonNull
    private String accountNum;
    @NonNull
    private Float sum;
}
