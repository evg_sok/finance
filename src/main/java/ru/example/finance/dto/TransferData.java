package ru.example.finance.dto;

import lombok.Data;
import lombok.NonNull;

@Data
public class TransferData {
    @NonNull
    private String toAccount;
    @NonNull
    private String fromAccount;
    @NonNull
    private Float sum;
}
