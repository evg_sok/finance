package ru.example.finance.configs;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.example.finance.converters.LocalDateTimeAdapter;

import java.time.LocalDateTime;

@Configuration
public class BeanConfigs {
    @Bean
    public GsonBuilder gsonBuilder() {
        return new GsonBuilder();
    }

    @Bean
    public Gson gsonManager(GsonBuilder gsonBuilder) {
        return gsonBuilder
                .registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter())
                .enableComplexMapKeySerialization().create();
    }
}
