package ru.example.finance.services;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.example.finance.dto.AccountData;
import ru.example.finance.dto.ChangeCashData;
import ru.example.finance.dto.TransferData;
import ru.example.finance.entities.Account;
import ru.example.finance.exceptions.AccountDataException;
import ru.example.finance.repositories.AccountRepository;

import java.util.Arrays;

@Service
@AllArgsConstructor
public class CashFlowService {
    private final AccountRepository accountRepository;
    private final Gson gson;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String topUp(ChangeCashData changeCashData) throws AccountDataException {
        if (changeCashData.getSum() <= 0) throw new AccountDataException("The amount must be greater than zero");
        Account account = accountRepository.findByNum(changeCashData.getAccountNum()).orElse(null);
        if (account == null) throw new AccountDataException("Account number not found");
        account.setCash(account.getCash() + changeCashData.getSum());
        return gson.toJson(new AccountData(accountRepository.saveAndFlush(account)));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean withdraw(ChangeCashData changeCashData) throws AccountDataException {
        if (changeCashData.getSum() <= 0) throw new AccountDataException("The amount must be greater than zero");

        Account account = accountRepository.findByNum(changeCashData.getAccountNum()).orElse(null);
        if (account == null) throw new AccountDataException("Account number not found");
        if (account.getCash() < changeCashData.getSum()) return false;

        account.setCash(account.getCash() - changeCashData.getSum());
        accountRepository.saveAndFlush(account);
        return true;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean transfer(TransferData transferData) throws AccountDataException {
        Account toAccount = null;
        Account fromAccount = null;

        //   Deadlock prevention
        if (transferData.getToAccount().hashCode() > transferData.getFromAccount().hashCode()) {
            toAccount = accountRepository.findByNum(transferData.getToAccount()).orElse(null);
            fromAccount = accountRepository.findByNum(transferData.getFromAccount()).orElse(null);
        } else {
            fromAccount = accountRepository.findByNum(transferData.getFromAccount()).orElse(null);
            toAccount = accountRepository.findByNum(transferData.getToAccount()).orElse(null);
        }

        if (toAccount == null) throw new AccountDataException("Destination account not found");
        if (fromAccount == null) throw new AccountDataException("Source account not found");

        if (fromAccount.getCash() < transferData.getSum()) return false;
        fromAccount.setCash(fromAccount.getCash() - transferData.getSum());
        toAccount.setCash(fromAccount.getCash() + transferData.getSum());
        accountRepository.saveAllAndFlush(Arrays.asList(fromAccount, toAccount));
        return true;
    }

    public boolean isValidTransfer(TransferData transferData) {
        return !transferData.getToAccount().equals(transferData.getFromAccount()) &&
                transferData.getSum() > 0;
    }

}
