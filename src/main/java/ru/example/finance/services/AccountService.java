package ru.example.finance.services;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.example.finance.dto.AccountData;
import ru.example.finance.entities.Account;
import ru.example.finance.exceptions.AccountDataException;
import ru.example.finance.repositories.AccountRepository;
import ru.example.finance.util.Helper;

@Service
@AllArgsConstructor
public class AccountService {

    private final AccountRepository accountRepository;
    private final Gson gson;

    public String create() {
        Account newAccount = accountRepository.saveAndFlush(new Account(Helper.generateAccount()));
        return gson.toJson(new AccountData(newAccount));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public String get(String accountNum) throws AccountDataException {
        Account account = accountRepository.findByNum(accountNum).orElse(null);
        if (account == null) throw new AccountDataException("Account number not found");

        return gson.toJson(new AccountData(account));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void remove(String accountNum) {
        Account account = accountRepository.findByNum(accountNum).orElse(null);
        if (account != null) accountRepository.delete(account);
    }
}
