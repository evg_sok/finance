package ru.example.finance.entities;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(name = "accounts", indexes = @Index(columnList = "num", unique = true))
@Data
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String num;

    @UpdateTimestamp
    private LocalDateTime updatedAt;

    private Float cash = 0f;

    public Account() {
    }

    public Account(String num) {
        this.num = num;
    }
}
