package ru.example.finance.controllers;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.example.finance.dto.ChangeCashData;
import ru.example.finance.dto.TransferData;
import ru.example.finance.exceptions.AccountDataException;
import ru.example.finance.services.CashFlowService;

@Controller
@RequestMapping("/cash-flow")
@AllArgsConstructor
public class CashFlowController {
    private static final Logger LOG = LoggerFactory.getLogger(CashFlowController.class);

    private final CashFlowService cashFlowService;

    @PostMapping("/transfer")
    public ResponseEntity<String> transfer(@RequestBody TransferData transferData) {
        try {
            if (cashFlowService.isValidTransfer(transferData)) {
                return ResponseEntity.ok(cashFlowService.transfer(transferData) ? "Success!" : "Operation not possible!");
            } else {
                throw new AccountDataException("Error! Incorrect data");
            }

        } catch (AccountDataException e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/top-up")
    public ResponseEntity<String> topUp(@RequestBody ChangeCashData changeCashData) {
        try {
            return ResponseEntity.ok(cashFlowService.topUp(changeCashData));
        } catch (AccountDataException e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/withdraw")
    public ResponseEntity<String> withdraw(@RequestBody ChangeCashData changeCashData) {
        try {
            return ResponseEntity.ok(cashFlowService.withdraw(changeCashData) ? "Success!" : "Operation not possible!");
        } catch (AccountDataException e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
