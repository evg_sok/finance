package ru.example.finance.controllers;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ru.example.finance.exceptions.AccountDataException;
import ru.example.finance.services.AccountService;


@Controller
@RequestMapping("/account")
@AllArgsConstructor
public class AccountController {

    private static final Logger LOG = LoggerFactory.getLogger(AccountController.class);

    private final AccountService accountService;

    @GetMapping(value = "/{num}")
    public ResponseEntity<String> get(@PathVariable("num") String accountNum) {
        try {
            return ResponseEntity.ok(accountService.get(accountNum));
        } catch (AccountDataException e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping
    public ResponseEntity<String> create() {
        try {
            return ResponseEntity.ok(accountService.create());
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping(value = "/{num}")
    public ResponseEntity<String> remove(@PathVariable("num") String accountNum) {
        try {
            accountService.remove(accountNum);
            return ResponseEntity.ok("Done");
        } catch (Exception e) {
            LOG.error(e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
