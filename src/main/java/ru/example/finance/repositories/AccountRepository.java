package ru.example.finance.repositories;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.stereotype.Repository;
import ru.example.finance.entities.Account;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

    /*
        Для атомарности выполнения операций пополнения/снятия/перевода средств, будет кидать
        блокировку на строку в пределах действия транзакции. LOCK_TIMEOUT=3000 в roperties
     */
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    Optional<Account> findByNum(String num);
}
